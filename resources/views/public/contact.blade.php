@extends('public/layout/layout')

@section('content')

<div class="container">

      <div class="bg-faded p-4 my-4">
        <hr class="divider">

        @if(session()->has('flash_message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Exito!</strong> {{session()->get('flash_message')}}
		</div>
		@endif

        <h2 class="text-center text-lg text-uppercase my-0">Contact
          <strong>Business Casual</strong>
        </h2>
        <hr class="divider">
        <div class="row">
          <div class="col-lg-8">
            <div class="embed-responsive embed-responsive-16by9 map-container mb-4 mb-lg-0">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3283.963891053708!2d-58.38519258477026!3d-34.60507458045907!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bccacf7e729e9b%3A0x5f49bec129321355!2sSarmiento+1154%2C+C1041AAX+CABA!5e0!3m2!1ses!2sar!4v1512873517859" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
          <div class="col-lg-4">
            <h5 class="mb-0">Teléfono:</h5>
            <div class="mb-4">1126993566</div>
            <h5 class="mb-0">Email:</h5>
            <div class="mb-4">
              <a href="mailto:name@example.com">erickorso@gmail.com</a>
            </div>
            <h5 class="mb-0">Dirección:</h5>
            <div class="mb-4">
              Sarmiento 1154
              <br>
              Buenos Aires
            </div>
          </div>
        </div>
      </div>

      <div class="bg-faded p-4 my-4">
        <hr class="divider">
        <h2 class="text-center text-lg text-uppercase my-0">Contact
          <strong>Form</strong>
        </h2>
        <hr class="divider">
        <form method="post" id="contact-form" action="{{ route('messages') }}" role="form">
        	{{csrf_field()}}
          <div class="row">
            <div class="form-group col-lg-4">
              <label class="text-heading">Name</label>
              <input type="text" class="form-control" name="name" required>
            </div>
            <div class="form-group col-lg-4">
              <label class="text-heading">Email Address</label>
              <input type="email" class="form-control" name="email" required>
            </div>
            <div class="form-group col-lg-4">
              <label class="text-heading">Phone Number</label>
              <input type="tel" class="form-control" name="phone" required>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-lg-12">
              <label class="text-heading">Message</label>
              <textarea class="form-control" name="body_message" rows="6" required></textarea >
            </div>
            <div class="form-group col-lg-12">
              <button type="submit" class="btn btn-secondary">Submit</button>
            </div>
          </div>
        </form>
      </div>

    </div>
    <!-- /.container -->

@stop