<!--para traer plantilla-->
@extends('public/layout/layout')

<!--para encerrar en wrap y mostrar despues de la platilla-->
@section('content')

	<div class="container">
		<hr>
		<div class="jumbotron">
	    	<h1 class="text-center">Página no Encontrada</h1>  
		</div>
	</div>
	<!-- /.container -->

<!--para cerrar wrapper-->
@stop