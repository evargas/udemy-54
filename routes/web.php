<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('public/home');
})->name('inicio');

Route::get('about', function () {
    return view('public/about');
})->name('nosotros');

Route::get('service', function () {
    return view('public/service');
})->name('servicios');

Route::get('contact', function () {
    return view('public/contact');
})->name('contacto');

Route::post('messages', function () {
	//enviar mensaje
    $data = request()->all();
	Mail::send("email/contact", $data, function($message) use ($data){
		$message->from($data['email'], $data['name'])
				->to('erick.max.vargas@gmail.com', 'Max')
				->subject('Contacto desde Laravel');
	});

	// enviar recibido
	return back()->with('flash_message', $data['name'].' tu mensaje fue recibido, en la brevedad posible nos pondremos en contacto contigo');
})->name('messages');